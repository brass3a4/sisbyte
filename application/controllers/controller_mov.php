<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controller_mov extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	public function index()
	{
		$this->load->view('movil/index_mov');
	}

}

/* End of file Controller_mov.php */
/* Location: ./application/controllers/Controller_mov.php */