<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ccontact extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('mcontact');
	}
	
	public function index()
	{	
		$this->load->view('vcabecera', NULL);
		$this->load->view('vindex', NULL);
		$this->load->view('vhome', NULL);
		$this->load->view('vweb', NULL);
		$this->load->view('vmobile', NULL);
		$this->load->view('vserver', NULL);
		$this->load->view('vabout', NULL);
		$this->load->view('vcontact', NULL);
		$this->load->view('vfooter', NULL);
	}

	public function sendMessage(){
		$contact = $this->input->post(); // devuelve todos los ítems POST con Filtrado XSS
		$contact['date'] = date("Y-m-d"); // Obtengo la fecha del servidor ;)
		if(isset($contact)){
			if($this->mcontact->setContact($contact)){
			// $this->index();
				redirect('ccontact');
			}else{
				echo (utf8_encode("¡Algo salió mal! :'("));
			}
		}
	}

}

/* End of file ccontacto.php */
/* Location: ./application/controllers/ccontacto.php */