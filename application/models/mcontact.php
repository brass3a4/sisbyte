<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mcontact extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	
	public function setContact($contact){
		if (!empty($contact)) {
			$this->db->insert('contact', $contact);
			if($this->db->affected_rows() > 0) {
				return true;
			} else {
				return false;
			}
		}
	}

}

/* End of file mcontactos.php */
/* Location: ./application/models/mcontactos.php */