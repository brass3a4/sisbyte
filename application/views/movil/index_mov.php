<!DOCTYPE html>
<html lang="">
	<head>
		<title>Sisbyte</title>
		<meta charset="UTF-8">
		<meta name=description content="">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		
		<link rel="stylesheet" type="text/css" href="../statics/css/estilo_moviles.css">
		<!-- Bootstrap CSS -->
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<script type="text/javascript" src="../statics/js/myCustomAnimations.js"></script>
	</head>
	<body>

		<nav class="navbar navbar-default" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Inicio</a>
			</div>
		
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Home</a></li>
					<li><a href="#">Movil</a></li>
					<li><a href="#">Web</a></li>
					<li><a href="pc_y_servidores">PC's yServidores</a></li>
					<li><a href="#">Contacto</a></li>
				</ul>
				<!-- <form class="navbar-form navbar-left" role="search">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Search">
					</div>
					<button type="submit" class="btn btn-default">Submit</button>
				</form>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#">Link</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else here</a></li>
							<li><a href="#">Separated link</a></li>
						</ul>
					</li>
				</ul> -->
			</div><!-- /.navbar-collapse -->
		</nav>

		<div class="mainImageContainer">
			<img class="homeImage" src="../statics/iconos/sisbyte.png">
		</div>
			<div id="myCarousel" class="carousel slide">
			  <ol class="carousel-indicators">
			    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			    <li data-target="#myCarousel" data-slide-to="1"></li>
			    <li data-target="#myCarousel" data-slide-to="2"></li>
			  </ol>
			  <!-- Carousel items -->
			  <div class="carrusel_Class carousel-inner">
		        <div class="item active">
		        	<img src="../statics/imagenes/serverPNG.png" class="img-responsive" alt="Image">
			        <!-- <div class="carousel-caption">
			              <h4>Primero</h4>
			              <p>Texto Ejemplo</p>
		            </div> -->
	          	</div>
				<div class="item">
			        <img src="../statics/imagenes/responsive.png" alt="">
			        <!-- <div class="carousel-caption">
			              <h4>Segundo</h4>
			              <p>Texto Ejemplo</p>
		            </div> -->
	          	</div>
	          	<div class="item">
			        <img src="../statics/imagenes/webDesign.png" alt="">
			        <!-- <div class="carousel-caption">
			              <h4>Tercero</h4>
			              <p>Texto Ejemplo</p>
		            </div> -->
	          	</div>
			  </div>

			  <!-- Carousel nav -->
			  <a class="carousel-control left" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
			  <a class="carousel-control right" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
			</div>

			<p>
			  	TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO

			  	TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO
			  	
			  	TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO

			  	TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO

			  	TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO
			  	
			  	TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO TEXTO
			</p>


			<div id="showFooter" class="footerSlider">
				<img class="footerSliderImage" src="../statics/imagenes/upArrow.png">

			</div>

			<div id="miFooter" class="el_footer">
				

			</div>

		<!-- jQuery -->
		<!--<script src="//code.jquery.com/jquery.js"></script>-->
		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
	</body>
</html>