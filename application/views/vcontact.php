	<div id="contact" class="formato" style="background: white;">
		<div style="maxwidth=1024px;" class="borde">
			<div class="large-8 small-8 columns">
				<h3>CONTACTO</h3>
				<hr>
				<div class="large-12 small-12 columns">
					<p>Nuestra empresa siempre esta dispuesta a inovar su aplicación, página web o servidor, nos ponemos a su disposición en los siguiente medios de comunicación, ansiosos de tener el honor de trabajar para usted.</p>
				</div>
				<div class="large-12 small-12 columns">
					<center>
						<div id="tablet" class="large-12 small-12 columns">
							<img id="imgTablet" src="<?=base_url()?>/statics/imagenes/tablet.png">
						</div>
						<div class="large-12 small-12 columns backAddress left">
							<div class="large-5 small-5 columns ">
								<h3>Dirección</h3>
								<p>María Luisa Martinez Mz. 44 Lt. 4</p>
								<p>Colonia: Carmen Serdam</p>
								<p>CP: 04910</p>
								<p>México D.F.</p>
							</div>
							<div class="large-7 small-7 columns">
								<iframe id="mapa" width="250px" height="285px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps?q=19.320314,-99.109879&amp;num=1&amp;t=m&amp;ie=UTF8&amp;ll=19.320314,-99.109879&amp;spn=0.001916,0.00284&amp;z=14&amp;output=embed"></iframe><br /><small><a href="https://www.google.com/maps?q=19.320314,-99.109879&amp;num=1&amp;t=m&amp;ie=UTF8&amp;ll=19.320314,-99.109879&amp;spn=0.001916,0.00284&amp;z=14&amp;source=embed" style="color:#0000FF;text-align:left">Ver mapa más grande</a></small>
							</div>
						</div>
					</center>
				</div>
			</div>
			<div class="large-4 small-4 columns">
				<div class="large-12 small-12 columns">
					<img id="imgCel" src="<?=base_url()?>/statics/imagenes/Scel.png">
				</div>
				<div id="cel" class="large-12 small-12 columns">
					<center>
						<a href="#" class="button" data-reveal-id="myModal" data-reveal>Contactanos</a>
					</center>
				</div>
				<div id="myModal" class="reveal-modal modal" data-reveal>
					<form data-abide action="<?=base_url()?>index.php/ccontact/sendMessage" method='post' name='process' accept-charset="utf-8" enctype="multipart/form-data">
						<fieldset>
							<div class="large-12 small-12 columns">
								<h3>Preguntanos</h3>
							</div>
							<div class="large-6 small-6 columns">
								<input name="name" type="text" placeholder="Nombre" required>
								<small class="error">Este campo es obligatorio.</small>
							</div>
							<div class="large-6 small-6 columns">
								<input name="lastname" type="text" placeholder="Apellidos" required>
								<small class="error">Este campo es obligatorio.</small>
							</div>
							<div class="large-6 small-6 columns">
								<input name="mail" type="email" placeholder="Correo" required>
								<small class="error">Este campo es obligatorio.</small>
							</div>
							<div class="large-6 small-6 columns">
								<input name="subject" type="text" placeholder="Asunto" required>
								<small class="error">Este campo es obligatorio.</small>
							</div>
							<div class="large-12 small-12 columns">
								<textarea name="message" placeholder="Mensaje" required></textarea>
								<small class="error">Este campo es obligatorio.</small>
							</div>
							<div class="large-12 small-12 columns">
								<input type="submit" class="button right [tiny small large]" value="Enviar">
							</div>
							<a class="close-reveal-modal">&#215;</a>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
<!-- </div> Fin de container vindex.php -->
