
<body>

	<!-- Librerias de validación de los formularios de Foundation -->
	<!-- Referencias: http://foundation.zurb.com/docs/components/abide.html -->
	<script type="text/javascript" src="<?=base_url(); ?>statics/foundation/js/vendor/jquery.js"></script>
	<script type="text/javascript" src="<?=base_url(); ?>statics/foundation/js/foundation/foundation.js"></script>
	<script type="text/javascript" src="<?=base_url(); ?>statics/foundation/js/foundation/foundation.abide.js"></script>
  	<script type="text/javascript" src="<?=base_url(); ?>statics/foundation/js/foundation/foundation.reveal.js"></script>
  	<script type="text/javascript" src="<?=base_url(); ?>statics/foundation/js/foundation/foundation.orbit.js"></script>

	<div id="menu">

		<div class="contentLogo" >
			<img class = "iconlogo" src="<?=base_url(); ?>statics/iconos/sisbyte.png">
		</div>


		<div class="centerButton">


			<div class="contentButton">
				<a onClick="goToByScroll('home'); return false;" class="medida" href="#">
						<img class="iconFlat" id="oneFlat" src="<?=base_url(); ?>statics/iconos/home.png">
						<img class="icon3D" id="one3D" src="<?=base_url(); ?>statics/iconos/home2.png">
						<p class="letterCaption" id="one" >HOME</p>
				</a>
			</div>

			<div class="contentButton">	
				<a onClick="goToByScroll('web'); return false;" class="medida" href="#">
						<img class="iconFlat" id="twoFlat" src="<?=base_url(); ?>statics/iconos/web.png">
						<img class="icon3D" id="two3D" src="<?=base_url(); ?>statics/iconos/web2.png">
						<p class="letterCaption" id="two" >WEB</p>
				</a>
			</div>

			<div class="contentButton">
				<a onClick="goToByScroll('mobile'); return false;" class="medida" href="#">
						<img class="iconFlat" id="threeFlat" src="<?=base_url(); ?>statics/iconos/moviles.png">
						<img class="icon3D" id="three3D" src="<?=base_url(); ?>statics/iconos/moviles2.png">
						<p class="letterCaption" id="three" >MOVILES</p>						
				</a>
			</div>

			<div class="contentButton">
				<a onClick="goToByScroll('server'); return false;" class="medida" href="#">
						<img class="iconFlat" id="fourFlat" src="<?=base_url(); ?>statics/iconos/servidores.png">
						<img class="icon3D" id="four3D" src="<?=base_url(); ?>statics/iconos/servidores2.png">
						<p class="letterCaption" id="four" >HARDWARE</p>
				</a>
			</div>

			<div class="contentButton">
				<a onClick="goToByScroll('about'); return false;" class="medida" href="#">
						<img class="iconFlat" id="fiveFlat" src="<?=base_url(); ?>statics/iconos/quienessomos.png">
						<img class="icon3D" id="five3D" src="<?=base_url(); ?>statics/iconos/quienessomos2.png">
						<p class="letterCaption" id="five" >SISBYTE</p>
				</a>
			</div>

			<div class="contentButton">
				<a onClick="goToByScroll('contact'); return false;" class="medida" href="#">
						<img class="iconFlat" id="sixFlat" src="<?=base_url(); ?>statics/iconos/contacto.png">
						<img class="icon3D" id="six3D" src="<?=base_url(); ?>statics/iconos/contacto2.png">
						<p class="letterCaption" id="six" >CONTACTO</p>
				</a>
			</div>

		</div>

	</div>

	<div class="container">
			
		
		
