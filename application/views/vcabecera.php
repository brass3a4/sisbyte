<!doctype html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Sisbyte</title>
	  	<!-- css -->
  	<link rel="stylesheet" type="text/css" href="<?=base_url();?>statics/foundation/css/normalize.css">
  	<link rel="stylesheet" type="text/css" href="<?=base_url();?>statics/foundation/css/foundation.css">
  	<link rel="stylesheet" type="text/css" href="<?=base_url();?>statics/css/estilo.css">
  	<link rel="stylesheet" type="text/css" href="<?=base_url();?>statics/css/estilocabecera.css">
  	<link rel="stylesheet" type="text/css" href="<?=base_url();?>statics/css/contact.css">
  	<link rel="stylesheet" type="text/css" href="<?=base_url();?>statics/css/web.css">
  	<link rel="stylesheet" type="text/css" href="<?=base_url();?>statics/css/footer.css">
  	<link rel="stylesheet" type="text/css" href="<?=base_url();?>statics/css/moviles.css">
  	<link rel="stylesheet" type="text/css" href="<?=base_url();?>statics/css/servidor.css">
  	<link rel="stylesheet" type="text/css" href="<?=base_url();?>statics/css/sisbyte.css">
	<!-- JS -->
	<script src="<?=base_url();?>statics/foundation/js/modernizr.js"></script>
	<script src="<?=base_url();?>statics/foundation/js/vendor/jquery.js"></script>
	<script src="<?=base_url();?>statics/foundation/js/foundation.min.js"></script>
	<script src="<?=base_url();?>statics/foundation/js/foundation/foundation.alert.js"></script>
	<script>
		function goToByScroll(id){
			$('html,body').animate({scrollTop: $("#"+id).offset().top},'slow');
		}
	</script>
</head>